﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2
{
    public partial class Database : System.Web.UI.Page
    {
        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();
            DataColumn index_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            index_col.ColumnName = "Line";
            index_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(index_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            List<string> database_code = new List<string>(new string[]{
                "-- FIND ALL COLUMS WHERE PRICE IS GREATER THAN 30",
                "SELECT * FROM Products WHERE Price > 30",
                "-- Finding the items that are priced between 50 and 60",
                "SELECT * FROM Products WHERE Price BETWEEN 50 AND 60 ",
                "-- the syntax would be:",
                "select (items) from (tablename) where condition",

            });

            int i = 0;
            foreach (string code_line in database_code)
            {
                coderow = codedata.NewRow();
                coderow[index_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;
                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            Code_Database.DataSource = ds;
            Code_Database.DataBind();

        }
    }
}