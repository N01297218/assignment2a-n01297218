﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace stylepractice
{
    public partial class Javascript : System.Web.UI.Page
    {
        public object Code_Javascript { get; set; }


        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();
            DataColumn index_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            index_col.ColumnName = "Line";
            index_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(index_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            
            List<string> code_javascript = new List<string>(new string[]{
                "<html>",
                "~<head>",
                "~</head>",
                "~<body>",
                "~~<main>",
                "~~~<p>This is my code!</p>",
                "~~</main>",
                "~</body>",
                "</html>"
            });

            int j = 0;
            foreach (string code_line in code_javascript)
            {
                coderow = codedata.NewRow();
                coderow[index_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                j++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            Code_Javascript.DataSource = ds;


            Code_Javascript.DataBind();



        }

       
    }
}