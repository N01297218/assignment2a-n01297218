﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Javascript.aspx.cs" Inherits="Assignment2.Javascript" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jsmain">
    <h2 class="jsheading">Javascript Introduction</h2>
    <p>
        JavaScript is one of the 3 languages all web developers must learn:

            1. HTML to define the content of web pages

            2. CSS to specify the layout of web pages

            3. JavaScript to program the behavior of web pages

        Web pages are not the only place where JavaScript is used. Many desktop and 
        server programs use JavaScript. Node.js is the best known. Some databases, like MongoDB and CouchDB, also use JavaScript as their programming language.
        JavaScript and Java are completely different languages, both in concept and design.
    </p>
    <h3 class="jsheading">Javascript Array Methods</h3>
    <p>Converting Arrays to string: toString()</p>
    <p>pop(): Removes the Last element</p>
    <p>unshift(): Adds new element at the front of the array.</p>
    <p>push():Returns the array length.</p></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SideBarOne" runat="server">
    <h2  class="jsheading1">Useful Examples:</h2>
    <div class="jspara">
    <p>var fruits = ["Banana", "Orange", "Apple", "Mango"];
        fruits.unshift("Lemon");    // Adds a new element "Lemon" to fruits
    </p>
    <p >
        var fruits = ["Banana", "Orange", "Apple", "Mango"];
        fruits.push("Kiwi");       //  Adds a new element ("Kiwi") to fruits
    </p>
    <p>
        var fruits = ["Banana", "Orange", "Apple", "Mango"];
        var x = fruits.pop();      // the value of x is "Mango"
    </p>
        
         <asp:DataGrid ID="Code_Javascript" runat="server" CssClass="code"
            GridLines="None" Width="380px" CellPadding="2" >
        <HeaderStyle Font-Size="Large" BackColor="#5d929c" ForeColor="White" />
        <ItemStyle BackColor="#cae4e1" ForeColor="#1a1d23"/>     
    </asp:DataGrid>
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideBarTwo" runat="server">
    <h2 class="jsheading2">Learning Resources:
    </h2>
    <nav id="jslinks">
    <p>
        <ul class="listjs">
            <li><a href="https://www.w3schools.com/js/default.asp">Basics</a></li>
            <li><a href="https://javascript.info/intro">Modern Tutorials</a></li>
            <li><a href="https://www.codecademy.com/learn/introduction-to-javascript">Codecadmey</a></li>
        </ul>
    </p></nav>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FooterBar" runat="server">
    <div class="myjs">
    <h2 class="jsnotes">My Notes:</h2>
    <p>
        The difference between for and while loop is:
        for is used when the number of iterations are known for the specific code.
    </p>
    <p>Syntax: for(intialise;condition;inc/dec);</p>
    <p>Syntax for while: while(i=0){execution; increment/decreement}</p>
    <p>An alert box is often used if you want to make sure information comes through to the user.
        When an alert box pops up, the user will have to click "OK" to proceed.</p>
    <p>A confirm box is often used if you want the user to verify or accept something.
        When a confirm box pops up, the user will have to click either "OK" or "Cancel" to proceed.
        If the user clicks "OK", the box returns true. If the user clicks "Cancel", the box returns false.</p>
    <p>A prompt box is often used if you want the user to input a value before entering a page.
        When a prompt box pops up, the user will have to click either "OK" or "Cancel" to proceed after entering an input value.
        If the user clicks "OK" the box returns the input value. If the user clicks "Cancel" the box returns null.</p></div>
</asp:Content>
