﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTML.aspx.cs" Inherits="Assignment2.HTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2 class="htmlheading"><strong>Introduction</strong></h2>
    <div class="htmlpara">
    <p>
        HTML is the standard markup language for creating Web pages.
    </p>
    <ol>
        <li>HTML stands for Hyper Text Markup Language</li>
        <li>HTML describes the structure of Web pages using markup</li>
        <li>HTML elements are the building blocks of HTML pages</li>
        <li>HTML elements are represented by tags</li>
        <li>HTML tags label pieces of content such as "heading", "paragraph", "table", and so on</li>
        <li>Browsers do not display the HTML tags, but use them to render the content of the page</li>
    </ol>
    </div>
    <h3 class="htmlheading1"> HTML Tags</h3>
    <p class="htmlpara1">HTML tags normally come in pairs like.The first tag in a pair is the start tag, the second tag is the end tag
    The end tag is written like the start tag, but with a forward slash inserted before the tag name</p>
    <h3 class="htmlheading1">Web Browsers</h3>
    <p class="htmlpara1">The purpose of a web browser (Chrome, IE, Firefox, Safari) is to read HTML documents and display them.
    The browser does not display the HTML tags, but uses them to determine how to display the document.</p>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SideBarOne" runat="server">
    <div class="htmlclass">
    <h2><strong>Examples:</strong></h2>
    <p> 
  
<asp:DataGrid ID="Code_html" runat="server" CssClass="code"
            GridLines="None" Width="380px" CellPadding="2" >
        <HeaderStyle Font-Size="Large" BackColor="#5d929c" ForeColor="White" />
        <ItemStyle BackColor="#cae4e1" ForeColor="#1a1d23"/>     
    </asp:DataGrid>
        
        
    </p>
    <p> <a href="https://www.w3schools.com/html/tryit.asp?filename=tryhtml_form_text">For code</a></p>
    
<%--example taken from online source--%></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideBarTwo" runat="server">
    <nav id="menu">
    <h2 class="htmlheading2">Useful Links:</h2>
    <ul class="html-links">
        <li><a href="https://www.w3schools.com/html/html_forms.asp">HTML Forms</a></li>
        <li><a href="https://www.w3schools.com/html/html_layout.asp">HTML Layout</a></li>
        <li><a href="https://www.w3schools.com/html/html_scripts.asp">HTML Javascript</a></li>
        <li><a href="https://www.w3schools.com/html/html_id.asp">HTML IDs</a></li>
        <li><a href="https://www.w3schools.com/html/default.asp">HTML Learnings All Concepts</a></li>
    </ul>
    </nav>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FooterBar" runat="server">
    <div class="myclass">
    <h2 class="htmlheading3">My Notes</h2>
        <div class="htmlpara2">
    <p>
        HTML tags will currently work whether they are capitals or lower case that is they are case sensitive.
        <h5>TAG</h5>
        the bit in-between the less than and greater than signs 
        <h5>Start and end tags</h5>
        most HTML tags come in pairs with one opening and one closing 
        <h5>Element</h5>
           everything inside the start and end tags including both tags 
        <h5>Self-closing tags</h5>
        A few tags do not come in pairs. They form an empty element on their own 
          <h5>Attributes</h5>
        within a start tag attributes provide some extra instructions to the browser 
        <h5>Value</h5>
        each attributes comes with a value after the equals sign. Get into the habit of putting the value in quotes as some have to be.
    </p>
            </div>
   </div>
</asp:Content>
