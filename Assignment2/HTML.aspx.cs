﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace stylepractice
{
    public partial class HTML : System.Web.UI.Page
    {
        public object Code_html { get; set; }

        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();
            DataColumn index_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            index_col.ColumnName = "Line";
            index_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(index_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            
            List<string> html_code = new List<string>(new string[]{
                "<html>",
                "~<head>",
                "~</head>",
                "~<body>",
                "~~<main>",
                "~~~<p>Welcome to Programming</p>",
                "~~</main>",
                "~</body>",
                "</html>"
            });

            int k = 0;
            foreach (string code_line in html_code)
            {
                coderow = codedata.NewRow();
                coderow[index_col.ColumnName] = k;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                k++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            Code_html.DataSource = ds;


            Code_html.DataBind();



        }
    }
}