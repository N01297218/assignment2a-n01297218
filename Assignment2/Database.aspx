﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assignment2.Database" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <h2 class="dbheading">SQL Operators</h2>
    <div class="dbpara">
    <p>
        An operator is a reserved word or a character used primarily in an SQL statement's WHERE clause to perform operation(s), 
        such as comparisons and arithmetic operations. 
        These Operators are used to specify conditions in an SQL statement and to serve as conjunctions for multiple conditions in a statement.
    </p>
    <h3>Types of operators:</h3>
    <ul>
        <li>Arithmetic operators</li>
        <li>Comparison operators</li>
        <li>Logical operators</li>
        <li>Operators used to negate conditions</li>
    </ul>
        </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="SideBarOne" runat="server">
    <h2 class="dbheading1">Useful Examples:</h2>
    <p class="dbpara1">
    

         <asp:DataGrid ID="Code_Database" runat="server" CssClass="code"
            GridLines="None" Width="380px" CellPadding="2" >
        <HeaderStyle Font-Size="Large" BackColor="#5d929c" ForeColor="White" />
        <ItemStyle BackColor="#cae4e1" ForeColor="#1a1d23"/>     
    </asp:DataGrid>

    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideBarTwo" runat="server">
    <div class="linkdiv">
    <h2>Learning Materials:</h2>
        <nav id="dblinks">
        <p>
    <ul class="dblist">
        <li><a href="https://www.w3schools.com/sql/sql_operators.asp">W3schools</a></li>
        <li><a href="https://www.tutorialspoint.com/sql/sql-operators.htm">Tutorials Point</a></li>
    </ul>
            </p></nav>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FooterBar" runat="server">
    <div class="dbnotes">
    <h2 class="dbheading3">My Notes:</h2>
        <div class="dbpara2">
    <p> 
        More features of SQL:
        <ul>
            <li>Joins</li>
            <li>Variables</li>
            <li>Functions</li>
            <li>Group by...Having</li>
            <li>Arithmatic operations</li>
        </ul>
    </p></div></div>
</asp:Content>
