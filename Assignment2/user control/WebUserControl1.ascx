﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebUserControl1.ascx.cs" Inherits="Assignment2.user_control.WebUserControl1" %>
<div id="side">
    <h3>Submit you doubts here:</h3>
    
    <div class="row">
        <div class="col-md-2 col-xs-12"><label class="formlabel">Name:</label></div>
        <div class="col-md-10 col-xs-12">
            <asp:TextBox runat="server" ID="name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="name"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label class="formlabel">Email:</label></div>
        <div class="col-md-10 col-xs-12">
            <asp:TextBox runat="server" ID="email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="email"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label class="formlabel">Doubts:</label></div>
        <div class="col-md-10 col-xs-12">
            <asp:DropDownList ID="doubt_topic" runat="server">
                <asp:ListItem>-Choose One-</asp:ListItem>
                <asp:ListItem>Theory</asp:ListItem>
                <asp:ListItem>Code</asp:ListItem>
                <asp:ListItem>Tutoring</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList>

        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label class="formlabel">Additional Notes:</label></div>
        <div class="col-md-10 col-xs-12">
            <asp:TextBox runat="server" TextMode="multiline" Columns="50" Rows="5"  ID="notes"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="notes"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <asp:Button runat="server" OnClick="submit_form" Text="Submit"/>
    </div>
    <asp:Label runat="server" ID="thank_you"></asp:Label>
</div>
